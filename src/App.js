import React, { Component } from 'react';

const UserContext = React.createContext();

const UserAvatar = ({ size }) => (
  <UserContext.Consumer>
    {user => (
      <img
        className={`user-avatar ${size || ''}`}
        alt="user avatar"
        src={user.avatar}
      />
    )}
  </UserContext.Consumer>
);

const UserStats = () => (
  <UserContext.Consumer>
    {user => (
      <div className="user-stats">
        <div>
          <UserAvatar user={user} />
          {user.name}
        </div>
        <div className="stats">
          <div>{user.followers} Followers</div>
          <div>Following {user.following}</div>
        </div>
      </div>
    )}
  </UserContext.Consumer>
);

const Nav = () => (
  <div className="nav">
    <UserAvatar size="small" />
  </div>
);

const Content = () => <div className="content">main content here</div>;

const Sidebar = () => (
  <div className="sidebar">
    <UserStats />
  </div>
);

const Body = () => (
  <div className="body">
    <Sidebar />
    <Content />
  </div>
);

class App extends Component {
  state = {
    user: {
      avatar:
        'https://secure.gravatar.com/avatar/d80db1e850e14c8b9eb98338da132978?s=46&d=identicon',
      name: 'Simo',
      followers: 1234,
      following: 123,
    },
  };

  render() {
    const { user } = this.state;
    return (
      <UserContext.Provider value={user}>
        <div className="app">
          <Nav />
          <Body />
        </div>
      </UserContext.Provider>
    );
  }
}

export default App;
